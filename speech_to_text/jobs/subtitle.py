import subprocess
import sys
import os
import argparse

##This process takes two command line arguments:
##First is the audio file path
##Second is is the subtitle folder path, under which there will be a subtitle.txt created 


def convert(audio_file_path, output_path):
    import speech_recognition as sr
    from pydub import AudioSegment
    from pydub.silence import split_on_silence
    import re
    import shutil

    sound = AudioSegment.from_mp3(audio_file_path)
    r = sr.Recognizer()
    chunks = split_on_silence(sound, min_silence_len=500, silence_thresh=sound.dBFS - 14, keep_silence=500)
    subtitle_file = os.path.join(output_path, "subtitle.txt")
    folder_name = os.path.join(output_path, "audio_chunks")
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    if not os.path.exists(folder_name):
        os.mkdir(folder_name)

    for i, audio_chunk in enumerate(chunks, start=1):
        chunk_file = os.path.join(folder_name, f"chunk{i}.wav")
        audio_chunk.export(chunk_file, format="wav")

    f = open(subtitle_file, "w")
    all_files = sorted(os.listdir(folder_name), key=lambda x: int(re.findall(r'\d+', x)[0]))
    for audio_file in all_files:
        with sr.AudioFile(os.path.join(folder_name, audio_file)) as source:
            audio_listened = r.record(source)
            try:
                text = r.recognize_google(audio_listened)
            except sr.UnknownValueError as e:
                f.write("Error")
                f.write("\n")
            else:
                f.write(text)
                f.write("\n")
    f.close()
    shutil.rmtree(folder_name)


def process():
    parser = argparse.ArgumentParser()
    parser.add_argument("audio_file_path")
    parser.add_argument("output_path")
    args = parser.parse_args()
    status = None
    try:
        audio_file_path = args.audio_file_path
        output_path = args.output_path
        convert(audio_file_path, output_path)

    except Exception:
        status = 1

    else:
        status = 0

    finally:
        print(status)
        sys.exit(status)


if __name__ == '__main__':
    process()
