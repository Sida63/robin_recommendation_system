import subprocess
import argparse
##The process takes more than 4 arguments:
##1. virtual env path
##2. the path of the script to be executed with this env
##3. optional args for the job
##4. optional args for the job
## example python /src/anaconda3/envs/robin script.py --job_arg1 arg1 --job_arg2 arg2
parser = argparse.ArgumentParser()
parser.add_argument("env_path")
parser.add_argument("script_path")
parser.add_argument("--job_arg1")
parser.add_argument("--job_arg2")
args = parser.parse_args()
interpreter_path = args.env_path + "/bin/python"
subprocess.run([interpreter_path, args.script_path, args.job_arg1, args.job_arg2])